#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib  import urlretrieve
from urllib2 import urlopen
from os.path import isfile
from time    import sleep
from re      import findall

remoteURI = 'http://kanjidamage.com'
listURI   = remoteURI + '/kanji'
outputURI = 'kanjidmg.txt'

# Sometimes romaji is written in full-width, which is
# inconsistent and takes lots of space.
def wideRomajiToASCII(str):
	mapping = [
		(r'Ａ', r'A'),
		(r'Ｂ', r'B'),
		(r'Ｃ', r'C'),
		(r'Ｄ', r'D'),
		(r'Ｅ', r'E'),
		(r'Ｆ', r'F'),
		(r'Ｇ', r'G'),
		(r'Ｈ', r'H'),
		(r'Ｉ', r'I'),
		(r'Ｊ', r'J'),
		(r'Ｋ', r'K'),
		(r'Ｌ', r'L'),
		(r'Ｍ', r'M'),
		(r'Ｎ', r'N'),
		(r'Ｏ', r'O'),
		(r'Ｐ', r'P'),
		(r'Ｑ', r'Q'),
		(r'Ｒ', r'R'),
		(r'Ｓ', r'S'),
		(r'Ｔ', r'T'),
		(r'Ｕ', r'U'),
		(r'Ｖ', r'V'),
		(r'Ｗ', r'W'),
		(r'Ｘ', r'X'),
		(r'Ｙ', r'Y'),
		(r'Ｚ', r'Z'),

		(r'ａ', r'a'),
		(r'ｂ', r'b'),
		(r'ｃ', r'c'),
		(r'ｄ', r'd'),
		(r'ｅ', r'e'),
		(r'ｆ', r'f'),
		(r'ｇ', r'g'),
		(r'ｈ', r'h'),
		(r'ｉ', r'i'),
		(r'ｊ', r'j'),
		(r'ｋ', r'k'),
		(r'ｌ', r'l'),
		(r'ｍ', r'm'),
		(r'ｎ', r'n'),
		(r'ｏ', r'o'),
		(r'ｐ', r'p'),
		(r'ｑ', r'q'),
		(r'ｒ', r'r'),
		(r'ｓ', r's'),
		(r'ｔ', r't'),
		(r'ｕ', r'u'),
		(r'ｖ', r'v'),
		(r'ｗ', r'w'),
		(r'ｘ', r'x'),
		(r'ｙ', r'y'),
		(r'ｚ', r'z'),

		(r'　', r' '),
		(r'（', r'('),
		(r'）', r')')
	]

	for k, v in mapping:
		k = k.decode('utf8')
		v = v.decode('utf8')
		str = str.replace(k, v)

	return str

# Onyomi are written in romaji, but usual consensus is to write
# them in Katakana.
def toKatakana(str):
	# Sorted by length
	patterns = [
		('tsyu', u'ツュ'),

		('shi', u'シ'),
		('chi', u'チ'),
		('tsu', u'ツ'),

		('kya', u'キャ'),
		('kyu', u'キュ'),
		('kyo', u'キョ'),

		('sha', u'シャ'),
		('shu', u'シュ'),
		('sho', u'ショ'),

		('cha', u'チャ'),
		('chu', u'チュ'),
		('cho', u'チョ'),

		('nya', u'ニャ'),
		('nyu', u'ニュ'),
		('nyo', u'ニョ'),

		('hya', u'ヒャ'),
		('hyu', u'ヒュ'),
		('hyo', u'ヒョ'),

		('mya', u'ミャ'),
		('myu', u'ミュ'),
		('myo', u'ミョ'),

		('rya', u'リャ'),
		('ryu', u'リュ'),
		('ryo', u'リョ'),

		('gya', u'ギャ'),
		('gyu', u'ギュ'),
		('gyo', u'ギョ'),

		('jya', u'ジャ'),
		('jyu', u'ジュ'),
		('jyo', u'ジョ'),

		('bya', u'ビャ'),
		('byu', u'ビュ'),
		('byo', u'ビョ'),

		('pya', u'ピャ'),
		('pyu', u'ピュ'),
		('pyo', u'ピョ'),

		('vya', u'ヴャ'),
		('vyu', u'ヴュ'),
		('vye', u'ヴェ'),
		('vyo', u'ヴョ'),

		('kye', u'キェ'),

		('gye', u'ギェ'),

		('kwa', u'クァ'),
		('kwi', u'クィ'),
		('kwe', u'クェ'),
		('kwo', u'クォ'),

		('gwa', u'グァ'),
		('gwi', u'グィ'),
		('gwe', u'グェ'),
		('gwo', u'グォ'),

		('she', u'シェ'),

		('che', u'チェ'),

		('tsa', u'ツァ'),
		('tsi', u'ツィ'),
		('tse', u'ツェ'),
		('tso', u'ツォ'),

		('tyu', u'テュ'),

		('dyu', u'ヂュ'),

		('nye', u'ニェ'),
		('hye', u'ヒェ'),
		('bye', u'ビェ'),
		('pye', u'ピェ'),

		('fya', u'フャ'),
		('fyu', u'フュ'),
		('fye', u'フィェ'),
		('fyo', u'フョ'),

		('mye', u'ミェ'),
		('rye', u'リェ'),

		('ka', u'カ'),
		('ki', u'キ'),
		('ku', u'ク'),
		('ke', u'ケ'),
		('ko', u'コ'),

		('sa', u'サ'),
		('si', u'シ'),
		('su', u'ス'),
		('se', u'セ'),
		('so', u'ソ'),

		('ta', u'タ'),
		('ti', u'チ'),
		('tu', u'ツ'),
		('te', u'テ'),
		('to', u'ト'),

		('na', u'ナ'),
		('ni', u'ニ'),
		('nu', u'ヌ'),
		('ne', u'ネ'),
		('no', u'ノ'),

		('ha', u'ハ'),
		('hi', u'ヒ'),
		('hu', u'フ'),
		('fu', u'フ'),
		('he', u'ヘ'),
		('ho', u'ホ'),

		('ma', u'マ'),
		('mi', u'ミ'),
		('mu', u'ム'),
		('me', u'メ'),
		('mo', u'モ'),

		('ya', u'ヤ'),
		('yu', u'ユ'),
		('yo', u'ヨ'),

		('ra', u'ラ'),
		('ri', u'リ'),
		('ru', u'ル'),
		('re', u'レ'),
		('ro', u'ロ'),

		('wa', u'ワ'),
		('wi', u'ヰ'),
		('we', u'ヱ'),
		('wo', u'ヲ'),

		('ga', u'ガ'),
		('gi', u'ギ'),
		('gu', u'グ'),
		('ge', u'ゲ'),
		('go', u'ゴ'),

		('za', u'ザ'),
		('ji', u'ジ'),
		('zi', u'ジ'),
		('zu', u'ズ'),
		('ze', u'ゼ'),
		('zo', u'ゾ'),

		('da', u'ダ'),
		('di', u'ヂ'),
		('du', u'ヅ'),
		('de', u'デ'),
		('do', u'ド'),

		('ba', u'バ'),
		('bi', u'ビ'),
		('bu', u'ブ'),
		('be', u'ベ'),
		('bo', u'ボ'),

		('pa', u'パ'),
		('pi', u'ピ'),
		('pu', u'プ'),
		('pe', u'ペ'),
		('po', u'ポ'),

		('nn', u'ン'),

		('ja', u'ジャ'),
		('ju', u'ジュ'),
		('jo', u'ジョ'),

		('yi', u'イィ'),
		('ye', u'イェ'),
		('wu', u'ウゥ'),

		('va', u'ヴァ'),
		('vi', u'ヴィ'),
		('vu', u'ヴ'),
		('ve', u'ヴェ'),
		('vo', u'ヴォ'),

		('je', u'ジェ'),

		('fa', u'ファ'),
		('fi', u'フィ'),
		('fe', u'フェ'),
		('fo', u'フォ'),

		('la', u'ラ'),
		('li', u'リ'),
		('lu', u'ル'),
		('le', u'レ'),
		('lo', u'ロ'),

		('va', u'ヴァ'),
		('vi', u'ヴィ'),
		('ve', u'ヴェ'),
		('vo', u'ヴォ'),

		(', ', u'、'),

		('a', u'ア'),
		('i', u'イ'),
		('u', u'ウ'),
		('e', u'エ'),
		('o', u'オ'),

		('n', u'ン'),
		('-', u'ー'),
		('.', u'。'),
		(' ', u' '),
		(',', u'、'),
		('/', u'／')
	]

	str = str.lower()
	length = len(str)

	i = 0
	while i < len(str):
		replaced = False
		for (romaji, katakana) in patterns:
			if str[i:i+len(romaji)] == romaji:
				str = str[0:i] + katakana + str[i+len(romaji):]
				i += len(katakana)
				replaced = True
				break
		if not replaced:
			print("Could not convert '" + str+ "' to katakana! Failed at index " + unicode(i))
			exit(1)

	return str


# There are various errors and inconsistencies in the descriptions
# of things and some things simply need to be retro-fitted.
def fixDescription(str):
	str = wideRomajiToASCII(str)
	str = str.strip()
	str = str.replace("\n", '') # Don't allow certain whitespaces,
	str = str.replace("\r", '') # since they break the output file format.
	str = str.replace("\t", '')
	str = str.replace('<P>', '<p>')
	str = str.replace('class="onyomi"',      "style='color: red'")
	str = str.replace('class="translation"', "style='color: blue'")
	str = str.replace('class="component"',   "style='color: green'")
	str = str.replace('&#x27;', "'")
	str = str.replace('<br>', '<br/>') # Fix bad XHTML
	str = str.replace('&lt;p&gt;&lt;img src= &quot;/visualaids/1akujin.jpg&quot;&gt;', '<img src="/visualaids/1akujin.jpg">')

	# Replace <p> tags without </p> with <br/>
	pos = 0
	while pos >= 0:
		pos = str.find('<p>', pos)
		if pos < 0:
			break
		end = str.find('</p>', pos)
		if end < 0:
			str = str[0:pos] + '<br/>' + str[pos+3:]
		pos = end+1

	# Strip Youtube-Objects
	pos = 0
	while pos >= 0:
		pos = str.find('<object', pos)
		if pos < 0:
			break

		start = pos

		endStr = '</object>'
		end = str.find(endStr, pos)
		if end < 0:
			break

		str = str[0:start] + str[end+len(endStr):]
		pos = end + 1

	# Replace image links with local variants
	uriStarts = ['/assets/visualaids/', '/visualaids/', '/assets/radsmall/', '/images/radREALLYsmall/', '/images/radsmall/']
	for uriStart in uriStarts:
		pos = 0
		uriStartLen = len(uriStart)
		uriEnd   = '"'

		while pos >= 0:
			pos = str.find(uriStart)
			if pos < 0:
				break

			start = pos
			pos += uriStartLen

			end = str.find(uriEnd, pos)
			if end < 0:
				print("Cannot find uri end. Bad HTML?")
				exit(1)
				break

			filename = str[pos:end]
			uri = remoteURI + uriStart + filename
			download(uri, filename)
			str = str[0:start] + filename + str[end:]
			pos = start + len(filename) + 1

	# Move usefulness stars to the next line
	str = str.replace('<span class="usefulness-stars"', '<br/><span class="usefulness-stars"')

	# Move certain links to the next line, like tags after usefulness stars
	str = str.replace('</span><a href="', '</span><br/><a href="')
	str = str.replace('href="/', 'href="' + remoteURI + '/')
	str = str.replace('<br/><br/>', '<br/>') # Fix double <br/>s, waste space

	return str

# Given a string with an offset which start with '<td>', return all
# content until '</td>' and an offset after '</td>'.
def readTD(text, offset):
	# Skip <td>
	if text[offset:offset+4] != '<td>':
		print("Unpexpected cell format. Website layout changed?")
		exit(1)
	offset += 4

	# Find content up to </td>
	endTagStr = '</td>\n'
	endTagOffset = text.find(endTagStr, offset)
	if endTagOffset < 0:
		print("Cannot find end of cell. Website layout changed?")
		exit(1)
	value = text[offset:endTagOffset]
	offset = endTagOffset + len(endTagStr)

	return (value, offset)

# Downloads a file to 'cache/' if it doesn't exist there.
def download(uri, filename):
	if not isfile('cache/' + filename):
		print("Downloading `" + uri + "'")
		urlretrieve(uri, 'cache/' + filename)

# Returns array of onyomi
def findOnyomi(html, start, end):
	if start >= 0 and end < 0:
		print("=== ERROR: Found onyomi start, but no end! ===")
		exit(1)
	elif start < 0: # Not found
		return []

	onyomi = []

	while start >= 0 and start < end:
		on = ''
		description = ''

		# Find on
		str = "<span class='onyomi'>"
		start = html.find(str, start, end)
		if start < 0:
			break
		start += len(str) # skip span node start
		str = '</span>'
		onEnd = html.find(str, start, end)
		if onEnd < 0:
			print("=== WARNING: Cannot find end of onyomi! ===")
			break
		on = html[start:onEnd].strip()

		# Remove unnecessary strings from exceptional Kanji
		on = on.replace(", but you don't need to learn it.", '.')
		on = on.replace(". You won't use this.", '')
		on = on.replace(" - you don't need this", '')
		on = on.replace(", also ", ',')
		on = on.replace(' -People forget about &quot;GE.&quot;', '')
		on = on.replace(' - same as the OTHER flower kanji', '')
		on = on.replace(' - but half the time just FU', ',FU')
		on = on.replace(',COCK', '')
		on = on.replace(" - don't bother.", '')
		on = on.replace(" (not so useful)", '')
		on = on.replace(" but not really", '')

		# There may be multiple onyomi
		toReplace = [' / ', ' /', '/ ', ' or ', ' and ', ' & ', ', ', 'none']
		for r in toReplace:
			on = on.replace(r, ',')
		on = toKatakana(on)

		# Find description
		start = onEnd + len(str)
		str = '<td>'
		start = html.find(str, start, end)

		if start >= 0:
			start += 5
			descriptionEnd = html.find('</td>', start, end)

			if descriptionEnd >= 0:
				description = html[start:descriptionEnd]
				description = fixDescription(description)

			start = descriptionEnd + 6

		onyomi.append((on, description))

		print("- Found onyomi '" + on.encode('utf-8') + '" with description:\n  ' + description.encode('utf-8'))

	return onyomi

# Returns array of kunyomi
def findKunyomi(html, start, end):
	if start >= 0 and end < 0:
		print("=== ERROR: Found kunyomi start, but no end! ===")
		exit(1)
	elif start < 0: # Not found
		return []

	kunyomi = []

	while start >= 0 and start < end:
		kun = ''
		description = ''

		# Find kun
		str = "<span class='kanji_character'>"
		start = html.find(str, start, end)
		if start < 0:
			break
		start += len(str) # skip span node start
		str = '</span>'
		kunEnd = html.find(str, start, end)
		if kunEnd < 0:
			print("=== WARNING: Cannot find end of kunyomi! ===")
			break
		kun = html[start:kunEnd].strip()

		# Find description
		start = kunEnd + len(str)
		str = '<td>'
		start = html.find(str, start, end)
		if start < 0:
			break
		start += len(str)
		str = '</td>'
		descriptionEnd = html.find(str, start, end)
		if descriptionEnd < 0:
			print("=== WARNING: Cannot find end of kunyomi description! ===")
			break
		description = html[start:descriptionEnd]
		description = fixDescription(description)

		kunyomi.append((kun, description))

		print("- Found kunyomi '" + kun.encode('utf-8') + '" with description:\n  ' + description.encode('utf-8'))

	return kunyomi

# Returns array of jukugo
def findJukugo(html, start, end):
	if start >= 0 and end < 0:
		print("=== ERROR: Found jukugo start, but no end! ===")
		exit(1)
	elif start < 0: # Not found
		return []

	jukugo = []

	while start >= 0 and start < end:
		juk = ''
		description = ''

		# Find jukugo
		str = "<span class='kanji_character'>"
		start = html.find(str, start, end)
		if start < 0:
			break
		start += len(str) # skip span node start
		str = '</span>'
		jukEnd = html.find(str, start, end)
		if jukEnd < 0:
			print("=== WARNING: Cannot find end of jukugo kanji! ===")
			break
		juk = html[start:jukEnd].strip()

		# Find description
		start = jukEnd + len(str)
		str = '<td>'
		start = html.find(str, start, end)
		if start < 0:
			print("=== WARNING: Cannot find start of jukugo description! ===")
			break
		start += len(str)
		str = '</td>'
		descriptionEnd = html.find(str, start, end)
		if descriptionEnd < 0:
			print("=== WARNING: Cannot find end of jukugo description! ===")
			break
		description = html[start:descriptionEnd]
		description = fixDescription(description)

		jukugo.append((juk, description))

		print("- Found jukugo '" + juk.encode('utf-8') + '" with description:\n  ' + description.encode('utf-8'))

	return jukugo

# Returns radicals, onyomi, kunyomi, jukugo, strokes, tags
def getKanjiInfo(kanjiURI):
	filename = kanjiURI.split("/")[-1]
	download(kanjiURI, filename)

	kanjiHTML   = open('cache/' + filename).read().decode('utf8')
	description = ''
	radicals    = []
	onyomi      = []
	kunyomi     = []
	jukugo      = []
	strokes     = 0
	tags        = []

	# Find radicals
	radicalsStartStr = "</span>\n</h1>"
	radicalsEndStr   = "\n</div>\n"

	radicalsStart = kanjiHTML.find(radicalsStartStr)
	if radicalsStart < 0:
		print("Cannot find start of radicals. Website layout changed?")
		exit(1)
	radicalsStart += len(radicalsStartStr)

	radicalsEnd = kanjiHTML.find(radicalsEndStr, radicalsStart)
	if radicalsEnd < 0:
		print("Cannot find end of radicals. Website layout changed?")
		exit(1)

	radicalsStr    = kanjiHTML[radicalsStart:radicalsEnd].strip()
	radicalsStrLen = len(radicalsStr)

	# Get the contents of the <a> node which is a Kanji or an <img> node
	# For artificial radicals a description of their positioning/meaning is given
	if radicalsStrLen > 0 and radicalsStr[0] == '(':
		description = radicalsStr[1:radicalsStr.find(')')]
		radicals.append( ('', description) )
		print("- Found an artificial radical with description '" + description + "'")
	elif radicalsStrLen > 0:
		valuePos = 0
		while valuePos < radicalsStrLen and valuePos >= 0:
			valuePos = radicalsStr.find(' class="component">', valuePos)
			if valuePos < 0:
				break

			valuePos += 19

			valueEnd = radicalsStr.find('</a>', valuePos)
			if valueEnd < 0:
				print("Cannot find radical end. Website layout changed?")
				exit(1)

			kanjiOrImg = radicalsStr[valuePos:valueEnd].strip()

			# If the radical is an image, change the link
			if kanjiOrImg[0:4] == '<img':
				srcStart = kanjiOrImg.find('src="')
				if srcStart < 0:
					print("Cannot find radical src start. Website layout changed?")
					exit(1)
				srcStart += 5

				srcEnd = kanjiOrImg.find('"', srcStart)
				if srcEnd < 0:
					print("Cannot find radical src end. Website layout changed?")
					exit(1)

				if kanjiOrImg[srcStart:srcEnd][0:5] == 'http:':
					src = kanjiOrImg[srcStart:srcEnd]
				else:
					src = remoteURI + kanjiOrImg[srcStart:srcEnd]
				filename = src.split('/')[-1]
				download(src, filename)

				kanjiOrImg = "<img src='" + filename + "'/>"

			valuePos = valueEnd+5

			# Find meaning
			meaningStart = radicalsStr.find('(', valuePos)
			if meaningStart < 0:
				print("Cannot find meaning start. Website layout changed?")
				exit(1)
			meaningStart += 1

			meaningEnd = radicalsStr.find(')', meaningStart)
			if meaningEnd < 0:
				print("Cannot find meaning end. Website layout changed?")
				exit(1)

			meaning = radicalsStr[meaningStart:meaningEnd].strip()

			# Check whether this is a same-onyomi-reading-radical
			sameOnStr = '<a href="/tags/63" class="label label-info">Same-ON</a>'
			sameOn = False
			if radicalsStr[meaningEnd+2:meaningEnd+2+len(sameOnStr)] == sameOnStr:
				kanjiOrImg = '<b>' + kanjiOrImg + '</b>'
				sameOn = True

			valuePos = meaningEnd + 1

			if sameOn:
				print("- Found same-onyomi-radical " + kanjiOrImg.encode('utf-8') + " with meaning " + meaning.encode('utf-8'))
			else:
				print("- Found radical: " + kanjiOrImg.encode('utf-8') + " with meaning " + meaning.encode('utf-8'))

			radicals.append((kanjiOrImg, meaning))

	# Find stroke count
	strokesEnd = kanjiHTML.find(' strokes</div>')
	if strokesEnd < 0:
		strokesEnd = kanjiHTML.find(' stroke</div>')

	if strokesEnd >= 0:
		strokesStart = kanjiHTML.rfind("<div>", 0, strokesEnd)
		if strokesStart >= 0:
			strokesStart += 5 # skip <div>
			strokes = int(kanjiHTML[strokesStart:strokesEnd])

	# Get the Kanji description
	descriptionStartStr = "<div class='description'>"
	descriptionStart = kanjiHTML.find(descriptionStartStr)
	if descriptionStart >= 0:
		descriptionEnd = kanjiHTML.find('</div>', descriptionStart)
		description = fixDescription(kanjiHTML[descriptionStart + len(descriptionStartStr) : descriptionEnd])
		print("- Found Kanji description:\n" + description.encode('utf-8'))

	# Find onyomi
	onyomiStart  = kanjiHTML.find('<h2>Onyomi</h2>')
	onyomiEnd    = kanjiHTML.find('</table>', onyomiStart)
	onyomi       = findOnyomi(kanjiHTML, onyomiStart, onyomiEnd)

	# Find kunyomi
	kunyomiStart = kanjiHTML.find('<h2>Kunyomi</h2>')
	kunyomiEnd   = kanjiHTML.find('</table>', kunyomiStart)
	kunyomi      = findKunyomi(kanjiHTML, kunyomiStart, kunyomiEnd)

	# Find jukugo
	jukugoStart  = kanjiHTML.find('<h2>Jukugo</h2>')
	jukugoEnd    = kanjiHTML.find('</table>', jukugoStart)
	jukugo       = findJukugo(kanjiHTML, jukugoStart, jukugoEnd)

	return (description, radicals, onyomi, kunyomi, jukugo, strokes, tags)

req  = urlopen(listURI)
html = req.read().decode('utf8')

# Find the table rows with kanji
startString = "<table class='table'>"
endString   = "</table>"

rowsStart = html.find(startString)
if rowsStart < 0:
	print("Cannot find start of kanji table. Website layout changed?")
	exit(1)

rowsStart += len(startString) + 1

rowsEnd = html.find(endString)
if rowsEnd < 0:
	print("Cannot find end of kanji table. Website layout changed?")
	exit(1)

outFile = open(outputURI, 'w')

i = rowsStart
wroteHeader = False
while i < rowsEnd:
	fields = {}

	# Skip <tr>\n
	if html[i:i+5] != "<tr>\n":
		print("Unpexpected row format. Website layout changed?")
		exit(1)
	i += 5

	kanjiNumber, i = readTD(html, i)
	kanjiOrImg,  i = readTD(html, i)
	meaning,     i = readTD(html, i)

	kanjiURIStart = kanjiOrImg.find('href="')
	if kanjiURIStart < 0:
		print("Cannot find Kanji URI start. Website layout changed?")
		exit(1)
	kanjiURIStart += 6

	kanjiURIEnd = kanjiOrImg.find('">', kanjiURIStart)
	if kanjiURIEnd < 0:
		print("Cannot find Kanji URI start. Website layout changed?")
		exit(1)
	kanjiURI = remoteURI + kanjiOrImg[kanjiURIStart:kanjiURIEnd]

	kanjiStart = kanjiURIEnd + 2
	kanjiEnd   = kanjiOrImg.find('</a>', kanjiStart)
	if kanjiEnd < 0:
		print("Cannot find Kanji end. Website layout changed?")
		exit(1)

	kanji = kanjiOrImg[kanjiStart:kanjiEnd].strip()

	# Detect if this is an image representing some radical
	if kanji[0:4] == '<img':
		# Find src attribute
		srcStart = kanji.find(' src="')
		if srcStart < 0:
			print("Cannot find src attribute for image of radical.")
			exit(1)
		srcStart += 6

		srcEnd = kanji.find('"', srcStart)
		if srcEnd < 0:
			print("Cannot find src attribute end for image of radical.")
			exit(1)

		if kanji[srcStart:srcEnd][0:5] == 'http:':
			src = kanji[srcStart:srcEnd]
		else:
			src = remoteURI + kanji[srcStart:srcEnd]
		filename = src.split('/')[-1]
		download(src, filename)

		fields['Kanji'] = u"<img src='" + filename + u"'/>"
	else:
		fields['Kanji'] = kanji

	fields['Number'] = kanjiNumber

	print("\nParsing Kanji #" + str(kanjiNumber) + ' ' + kanji.encode('utf-8'))
	description, radicals, onyomi, kunyomi, jukugo, strokes, tags = getKanjiInfo(kanjiURI)

	fields['Strokes']     = str(strokes)
	fields['Description'] = description
	fields['Meaning']     = meaning

	radicalCount = len(radicals)
	if radicalCount > 0:
		radicalsStr = ""
		for j in range(0, radicalCount):
			if j > 0:
				radicalsStr += ' + '
			radicalsStr += radicals[j][0] + ' (' + radicals[j][1] + ')'
		fields['Radicals'] = radicalsStr
	else:
		fields['Radicals'] = ''

	onyomiCount = len(onyomi)
	if onyomiCount > 0:
		onyomiStr = ""
		for onyomiIndex in range(0, onyomiCount):
			onyomiStr += "<div><a style='color: red'>"+ onyomi[onyomiIndex][0] + '</a><br/>' + onyomi[onyomiIndex][1] + '</div>'
		fields['Onyomi'] = onyomiStr
	else:
		fields['Onyomi'] = ''

	kunyomiCount = len(kunyomi)
	if kunyomiCount > 0:
		kunyomiStr = ""
		for kunyomiIndex in range(0, kunyomiCount):
			kunyomiStr += '<p>' + kunyomi[kunyomiIndex][0] + '<br/>' + kunyomi[kunyomiIndex][1] + '</p>'
		fields['Kunyomi'] = kunyomiStr
	else:
		fields['Kunyomi'] = ''

	jukugoCount = len(jukugo)
	if jukugoCount > 0:
		jukugoStr = ""
		for jukugoIndex in range(0, jukugoCount):
			# The description is generally in <p>, but in case it's not, let's put a <br/> there
			description = jukugo[jukugoIndex][1]
			descriptionLen = len(description)
			if descriptionLen > 0 and description[0:2] != '<p' and description[0:4] != '<div':
				description = '<br/>' + description
			jukugoStr += "<div style='margin:20px 0'>" + jukugo[jukugoIndex][0] + description + '</div>'

		fields['Jukugo'] = jukugoStr
	else:
		fields['Jukugo'] = ''

	# Format output
	a = "<span style='font-family: KanjiStrokeOrders; font-size: 400%'>" + fields['Kanji'] + '</span>\t'
	if len(fields['Radicals']) > 0:
		a += "<div>"  + fields['Radicals']    + "</div>"
	if len(fields['Meaning']) > 0:
		a += "<div style='color: #46A546'>"   + fields['Meaning'] + "</div>"
	if len(fields['Onyomi']) > 0:
		a += "<div>"  + fields['Onyomi']      + "</div>"
	if len(fields['Kunyomi']) > 0:
		a += "<div>"  + fields['Kunyomi']     + "</div>"
	if len(fields['Onyomi']) > 0:
		a += "<div>"  + fields['Jukugo']      + "</div>"
	if len(fields['Number']) > 0:
		a += "<div>#" + fields['Number']      + "</div>"
	if len(fields['Description']) > 0:
		a += "<div>"  + fields['Description'] + "</div>"
	a += "\n"

	# Reverse card
	a += fields['Number'] + "<div style='color: #46A546'>" + fields['Meaning'] + "</div>" + fields['Strokes'] + " strokes\t"
	if len(fields['Kanji']) > 0:
		a += "<div style='font-family: KanjiStrokeOrders; font-size: 400%'>" + fields['Kanji'] + '</div>'
	if len(fields['Radicals']) > 0:
		a += "<div>"  + fields['Radicals']    + "</div>"
	if len(fields['Onyomi']) > 0:
		a += "<div>"  + fields['Onyomi']      + "</div>"
	if len(fields['Kunyomi']) > 0:
		a += "<div>"  + fields['Kunyomi']     + "</div>"
	if len(fields['Jukugo']) > 0:
		a += "<div>"  + fields['Jukugo']      + "</div>"
	if len(fields['Number']) > 0:
		a += "<div>#" + fields['Number']      + "</div>"
	if len(fields['Description']) > 0:
		a += "<div>"  + fields['Description'] + "</div>"
	a += "\n"

	outFile.write(a.encode('utf-8'))

	# Skip </tr>\n
	if html[i:i+6] != "</tr>\n":
		print("Unpexpected row end format. Website layout changed?")
		exit(1)
	i += 6
